# simple-jbdd

This project is a simple BDD framework.

The project uses the [Spock Framework Example Project](https://github.com/spockframework/)

## Prerequisites

- JDK 17 or higher

- Maven use `mvnw` wrapper

- Gradle use `gradlew` wrapper

## Building with Gradle

Downloaded files (including the Gradle distribution itself) will be stored in the Gradle user home directory (typically **user_home**`/.gradle`).

Type: test

```shell
./gradlew clean test
```

Type: build

```shell
./gradlew clean build
```

Type: run with asserts

```shell
java -ea -jar ./build/libs/simple_jbdd-0.13.5-all.jar
```

## Building with Maven

Downloaded files will be stored in the local Maven repository (typically **user_home**`/.m2/repository`).

Type: test

```shell
./mvnw clean test
```

Type: build


```shell
./mvnw clean package
```

Type: run with asserts

```shell
java -ea -jar ./target/simple_jbdd-0.13.5.jar
```

## Author

Kostiantyn Klochko (c) 2023

## License

Under the GNU Lesser General Public License v3.0 or later.

