package space.kklochko.simple_jbdd.test_examples.samples

import space.kklochko.simple_jbdd.test_examples.samples.Sum
import spock.lang.Specification

class SumSpec extends Specification {
    def "Adding two numbers"() {
        expect: "Two plus three is five."
        5 == Sum.sum(2, 3);
    }
}

