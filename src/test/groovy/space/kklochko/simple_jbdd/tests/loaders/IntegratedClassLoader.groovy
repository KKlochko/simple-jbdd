package space.kklochko.simple_jbdd.tests.loaders

import spock.lang.Narrative
import spock.lang.Specification
import spock.lang.Subject
import spock.lang.Title

@Narrative("""The loader must return a test classes or null, so
those tests check if responses are right.
""")
@Title("Integrated tests for ClassLoader.")
class IntegratedClassLoader extends Specification {
    def "The classes have been loaded."() {
        given: "I have a name class loader"
        def nameLoader = new NameClassLoader()

        and: "I have a loader for classes"
        @Subject
        def loader = new ClassLoader(nameLoader)

        when: "The classes were loaded"
        def classes = loader.load()

        then: "Checking that the test result is expected"
        11 == classes.size()
    }

    def "The classes must be represent the Test interface."() {
        given: "I have a name class loader"
        def nameLoader = new NameClassLoader()

        and: "I have a loader for classes"
        @Subject
        def loader = new ClassLoader(nameLoader)

        when: "The classes were loaded"
        def classes = loader.load()

        then: "Checking that the test result is expected"
        classes.every({ space.kklochko.simple_jbdd.tests.Test.class.isAssignableFrom(it) })
    }
}

