package space.kklochko.simple_jbdd.tests.loaders

import spock.lang.Narrative
import spock.lang.Specification
import spock.lang.Subject
import spock.lang.Title

@Narrative("""The loader must return a test class names or null, so
those tests check if responses are right.
""")
@Title("Integrated tests for NameClassLoader.")
class IntegratedNameClassLoader extends Specification {
    def "The class names have been loaded."() {
        given: "I have a class loader"
        @Subject
        def loader = new NameClassLoader()

        when: "The class names were loaded"
        def classNames = loader.load()

        then: "Checking that the test result is expected"
        11 == classNames.size()
    }
}

