package space.kklochko.simple_jbdd.tests.loaders

import space.kklochko.simple_jbdd.test_examples.tests.SimpleThenTest
import spock.lang.Narrative
import spock.lang.Specification
import spock.lang.Subject
import spock.lang.Title

@Narrative("""The loader must return a class or null, so
those tests check if responses are right.
""")
@Title("Integrated tests for SingleClassLoader.")
class IntegratedSingleClassLoader extends Specification {
    def "The class has been loaded."() {
        given: "I have a class loader"
        @Subject
        def loader = new SingleClassLoader(className)

        when: "The class was loaded"
        def aClass = loader.load()

        then: "Checking that the test result is expected"
        aClass == expectedClass

        where: "Possible variants of tests"
        className                                                       || expectedClass
        "fourHundredFour"                                               || null
        "space.kklochko.simple_jbdd.test_examples.tests.SimpleThenTest" || SimpleThenTest
    }
}

