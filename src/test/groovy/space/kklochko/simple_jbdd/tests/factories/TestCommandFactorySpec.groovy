package space.kklochko.simple_jbdd.tests.factories

import space.kklochko.simple_jbdd.test_examples.tests.SimpleEmptyTest
import space.kklochko.simple_jbdd.test_examples.tests.SimpleGivenGivenWhenWhenThenThenTest
import space.kklochko.simple_jbdd.test_examples.tests.SimpleGivenWhenThenTest
import space.kklochko.simple_jbdd.test_examples.tests.SimpleThenTest
import space.kklochko.simple_jbdd.test_examples.tests.SimpleThenTestWithoutTitle
import space.kklochko.simple_jbdd.tests.factories.TestCommandFactory
import spock.lang.Narrative;
import spock.lang.Specification
import spock.lang.Subject
import spock.lang.Title

@Narrative("""The factory must generate a test command, so
those tests check if factory generate them right.
""")
@Title("Unit tests for TestCommandFactory")
class TestCommandFactorySpec extends Specification {
    def "The command have the same block count."() {
        given: "I have a TestCommandFactory object"
        @Subject
        def factory = new TestCommandFactory();

        when: "If a test command have blocks"
        def map = factory.getTestMethods(testClass.getClass())

        def count = map.values()
                .stream()
                .map {it.size()}
                .reduce(0) { acc, num -> acc + num }

        then: "the command have the expected block count"
        expectedBlockCount == count

        where:
        testClass                                  || expectedBlockCount
        new SimpleEmptyTest()                      || 0
        new SimpleThenTest()                       || 1
        new SimpleGivenWhenThenTest()              || 3
        new SimpleGivenGivenWhenWhenThenThenTest() || 6
    }

    def "createTest() sets the title to a class name if no Title annotation."() {
        given: "I have a test factory"
        @Subject
        def factory = new TestCommandFactory()

        and: "I have a test class without Title"
        def testClass = new SimpleThenTestWithoutTitle()

        when: "I build a test"
        def test = factory.create(testClass)

        then: "The test have title as the test class"
        testClass.class.getName() == test.getLabels()['Title']
    }

    def "createTest() sets the title to a value of the Title annotation."() {
        given: "I have a test factory"
        @Subject
        def factory = new TestCommandFactory()

        and: "I have a test class with Title"
        def testClass = new SimpleThenTest()

        and: "and the title of the annotation"
        def titleClass = space.kklochko.simple_jbdd.annotations.Title.class
        def title = testClass.getClass().getAnnotation(titleClass).value()

        when: "I build a test"
        def test = factory.create(testClass)

        then: "The test have title as the test class"
        title == test.getLabels()['Title']
    }

    def "createTest() build a TestCommand that contains the expected blocks."() {
        given: "I have a test factory"
        @Subject
        def factory = new TestCommandFactory()

        when: "I build a test"
        def test = factory.create(testClass)

        then: "The test have the expected blocks"
        expectedBlocks.toSet() == test.getLabels().keySet()

        where:
        testClass                                  || expectedBlocks
        new SimpleThenTest()                       || ['Title', 'Then']
        new SimpleGivenWhenThenTest()              || ['Title', 'Given', 'When', 'Then']
        new SimpleGivenGivenWhenWhenThenThenTest() || ['Title', 'Given', 'When', 'Then']
    }
}

