package space.kklochko.simple_jbdd.tests.factories.meta

import space.kklochko.simple_jbdd.test_examples.tests.SimpleGivenWhenThenTest
import space.kklochko.simple_jbdd.test_examples.tests.SimpleThenTest
import space.kklochko.simple_jbdd.tests.commands.AbstractTestCommand
import space.kklochko.simple_jbdd.tests.commands.decorators.BlockDecorator
import space.kklochko.simple_jbdd.tests.factories.TestCommandFactory
import spock.lang.Narrative;
import spock.lang.Specification
import spock.lang.Subject
import spock.lang.Title

@Narrative("""The factory must generate a test command metadata, so
those tests check if the factory generate it right.
""")
@Title("Unit tests for TestCommandMetaDataFactory")
class TestCommandMetaDataFactorySpec extends Specification {
    def "If the map of methods have one test block."() {
        given: "I have a simple test with only Then block and its command."
        def test = Mock(AbstractTestCommand) {
            getType() >> "Title"
            getLabel() >> "A simple test"
        }

        def command = GroovyMock(BlockDecorator) {
            getType() >> "Then"
            getLabel() >> "Then block"
            getCommand() >> test
        }

        and: "I have a TestCommandMetaDataFactory object"
        @Subject
        def factory = new TestCommandMetaDataFactory(command)

        when: "Parse the command to get metadata"
        def metadata = factory.create()

        then: "The metadata have only two labels for Title and Then"
        2 == metadata.size()

        and: "The format and the order must be the same"
        metadata == [
                new AbstractMap.SimpleEntry<>("Title", "A simple test"),
                new AbstractMap.SimpleEntry<>("Then", "Then block")
        ]
    }

    def "If the map of methods have GivenWhenThen blocks."() {
        given: "I have a simple test with only Then block and its command."
        def test = Mock(AbstractTestCommand) {
            getType() >> "Title"
            getLabel() >> "A simple test"
        }

        and: "Bootstrapping the test command"
        def then = GroovyMock(BlockDecorator) {
            getType() >> "Then"
            getLabel() >> "Then block"
            getCommand() >> test
        }

        def when = GroovyMock(BlockDecorator) {
            getType() >> "When"
            getLabel() >> "When block"
            getCommand() >> then
        }

        def command = GroovyMock(BlockDecorator) {
            getType() >> "Given"
            getLabel() >> "Given block"
            getCommand() >> when
        }

        and: "I have a TestCommandMetaDataFactory object"
        @Subject
        def factory = new TestCommandMetaDataFactory(command)

        when: "Parse the command to get metadata"
        def metadata = factory.create()

        then: "The metadata have only two labels for Title and Then"
        4 == metadata.size()

        and: "The format and the order must be the same"
        metadata == [
                new AbstractMap.SimpleEntry<>("Title", "A simple test"),
                new AbstractMap.SimpleEntry<>("Given", "Given block"),
                new AbstractMap.SimpleEntry<>("When", "When block"),
                new AbstractMap.SimpleEntry<>("Then", "Then block")
        ]
    }
}

