package space.kklochko.simple_jbdd.tests.factories.meta

import space.kklochko.simple_jbdd.test_examples.tests.SimpleFailedThenTest
import space.kklochko.simple_jbdd.test_examples.tests.SimpleGivenWhenThenTest
import space.kklochko.simple_jbdd.test_examples.tests.SimpleThenTest
import space.kklochko.simple_jbdd.test_examples.tests.SimpleThenTestWithoutTitle
import space.kklochko.simple_jbdd.tests.commands.AbstractTestCommand
import space.kklochko.simple_jbdd.tests.commands.decorators.BlockDecorator
import space.kklochko.simple_jbdd.tests.factories.TestCommandFactory
import space.kklochko.simple_jbdd.tests.runners.SimpleTestRunner
import spock.lang.Narrative
import spock.lang.Specification
import spock.lang.Subject
import spock.lang.Title

@Narrative("""The factory must generate a test command metadata, so
those tests check if the factory generate it right.
""")
@Title("Integrated tests for TestCommandMetaDataFactory")
class IntegratedTestCommandMetaDataFactorySpec extends Specification {
    def "Metadata have  the map of methods have one test block."() {
        given: "I have a command factory"
        def commandFactory = new TestCommandFactory();

        and: "I have a command of a test"
        def command = commandFactory.create(testObject)

        and: "I have a TestCommandMetaDataFactory object"
        @Subject
        def factory = new TestCommandMetaDataFactory(command)

        when: "Parse the command to get metadata"
        def metadata = factory.create()

        then: "the metadata have only two labels for Title and Then"
        expectedCount == metadata.size()

        where: "Possible variants of tests"
        testObject                       || expectedCount
        new SimpleThenTest()             || 2
        new SimpleGivenWhenThenTest()    || 4
        new SimpleThenTestWithoutTitle() || 2
        new SimpleFailedThenTest()       || 2
    }
}

