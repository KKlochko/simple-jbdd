package space.kklochko.simple_jbdd.tests.factories

import space.kklochko.simple_jbdd.test_examples.tests.SimpleThenTest
import space.kklochko.simple_jbdd.test_examples.tests.SimpleThenTestWithoutTitle
import space.kklochko.simple_jbdd.tests.Test
import space.kklochko.simple_jbdd.tests.commands.decorators.BlockDecorator
import spock.lang.Narrative
import spock.lang.Specification
import spock.lang.Subject
import spock.lang.Title

@Narrative("""The factory must generate a queue of the test commands, so
those tests check if factory generate them right.
""")
@Title("Unit tests for TestCommandQueueFactory")
class TestCommandQueueFactorySpec extends Specification {
    def "Factory generate the queue and its size is 2."() {
        given: "I have a list of tests"
        def tests = [Mock(SimpleThenTest), Mock(SimpleThenTestWithoutTitle)]

        and: "I have a test factory"
        def testFactory = Stub(TestCommandFactory)
        testFactory.create() >>> [Mock(BlockDecorator<Test>)]*2

        and: "I have the factory"
        @Subject
        def factory = new TestCommandQueueFactory(tests, testFactory)

        when: "Factory build the queue"
        def queue = factory.create()

        then: "the size is 2"
        2 == queue.size()
    }
}

