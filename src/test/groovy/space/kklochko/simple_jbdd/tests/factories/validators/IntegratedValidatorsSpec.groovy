package space.kklochko.simple_jbdd.tests.factories.validators

import space.kklochko.simple_jbdd.test_examples.tests.HasMethodArgumentsTest
import space.kklochko.simple_jbdd.test_examples.tests.HasNonVoidMethodTest
import space.kklochko.simple_jbdd.test_examples.tests.HasPrivateMethodTest
import space.kklochko.simple_jbdd.test_examples.tests.SimpleEmptyTest
import space.kklochko.simple_jbdd.test_examples.tests.SimpleGivenWhenThenTest
import space.kklochko.simple_jbdd.tests.factories.TestCommandFactory
import spock.lang.Narrative
import spock.lang.Specification
import spock.lang.Subject
import spock.lang.Title

@Narrative("""The validators must return true if a test class is invalid, so
those tests check if validators return the right result.
""")
@Title("Integrated tests for Validators")
class IntegratedValidatorsSpec extends Specification {
    def "Validator return isInvalid"() {
        given: "I have a factory and metadata"
        def factory = new TestCommandFactory()
        def metaData = factory.getTestMethods(testClass)

        when: "Validating the test"
        def isInvalid = validator.validate(metaData)

        then: "The status must be expected"
        isInvalid == expectedStatus

        where: "Possible variants of tests"
        validator                              | testClass                     || expectedStatus
        new HasMethodsWithArgumentsValidator() | HasMethodArgumentsTest.class  || true
        new HasMethodsWithArgumentsValidator() | SimpleGivenWhenThenTest.class || false
        new HasNonVoidMethodsValidator()       | HasNonVoidMethodTest.class    || true
        new HasMethodsWithArgumentsValidator() | SimpleGivenWhenThenTest.class || false
        new HasPrivateMethodsValidator()       | HasPrivateMethodTest.class    || true
        new HasMethodsWithArgumentsValidator() | SimpleGivenWhenThenTest.class || false
        new EmptyValidator()                   | SimpleEmptyTest.class         || true
        new HasMethodsWithArgumentsValidator() | SimpleGivenWhenThenTest.class || false
    }

    def "Validator return the error message"() {
        given: "I have a factory and metadata"
        def factory = new TestCommandFactory()
        def metaData = factory.getTestMethods(testClass)

        when: "Validating the test and checking the message"
        validator.validate(metaData)
        def message = validator.getMessage()

        then: "The message must be expected"
        message ==~ expectedMessage

        where: "Possible variants of tests"
        validator                              | testClass                     || expectedMessage
        new HasMethodsWithArgumentsValidator() | HasMethodArgumentsTest.class  || "ERROR!!! Methods must have no arguments!!! Check methods: (given|setupArguments), (given|setupArguments)!!!"
        new HasMethodsWithArgumentsValidator() | SimpleGivenWhenThenTest.class || "ok"
        new HasNonVoidMethodsValidator()       | HasNonVoidMethodTest.class    || "ERROR!!! Methods must have the void type!!! Check methods: then!!!"
        new HasMethodsWithArgumentsValidator() | SimpleGivenWhenThenTest.class || "ok"
        new HasPrivateMethodsValidator()       | HasPrivateMethodTest.class    || "ERROR!!! Methods must be public!!! Check method: given!!!"
        new HasMethodsWithArgumentsValidator() | SimpleGivenWhenThenTest.class || "ok"
        new EmptyValidator()                   | SimpleEmptyTest.class         || "ERROR!!! No blocks!!!"
        new HasMethodsWithArgumentsValidator() | SimpleGivenWhenThenTest.class || "ok"
    }
}

