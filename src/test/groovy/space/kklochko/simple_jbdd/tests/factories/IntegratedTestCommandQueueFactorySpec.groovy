package space.kklochko.simple_jbdd.tests.factories

import space.kklochko.simple_jbdd.test_examples.tests.SimpleGivenWhenThenTest
import space.kklochko.simple_jbdd.test_examples.tests.SimpleThenTest
import space.kklochko.simple_jbdd.test_examples.tests.SimpleThenTestWithoutTitle
import space.kklochko.simple_jbdd.tests.Test
import spock.lang.Narrative
import spock.lang.Specification
import spock.lang.Subject
import spock.lang.Title

@Narrative("""The factory must generate a queue of the test commands, so
those tests check if factory generate them right.
""")
@Title("Integrated tests for TestCommandQueueFactory")
class IntegratedTestCommandQueueFactorySpec extends Specification {
    def "Factory generate the queue and its size is 2."() {
        given: "I have a list of tests"
        def tests = [new SimpleThenTest(), new SimpleThenTestWithoutTitle()]

        and: "I have a test factory"
        def testFactory = new TestCommandFactory()

        and: "I have a factory"
        @Subject
        def factory = new TestCommandQueueFactory(tests, testFactory)

        when: "Factory build the queue"
        def queue = factory.create()

        then: "the size is 2"
        2 == queue.size()
    }

    def "Factory generate the queue and the commands and test order is the same."() {
        given: "I have a list of tests with first methods [check, setup]"
        def tests = [new SimpleThenTest(), new SimpleGivenWhenThenTest()]

        and: "I have a test factory"
        def testFactory = new TestCommandFactory()

        and: "I have a factory"
        @Subject
        def factory = new TestCommandQueueFactory(tests, testFactory)

        when: "Factory build the queue"
        def queue = factory.create()

        then: "the order is the same"
        queue[0].method.getName() == "check"
        queue[1].method.getName() == "setup"
    }
}

