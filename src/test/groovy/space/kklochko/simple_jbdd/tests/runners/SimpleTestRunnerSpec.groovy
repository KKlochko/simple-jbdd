package space.kklochko.simple_jbdd.tests.runners

import space.kklochko.simple_jbdd.tests.commands.SimpleTestCommand
import space.kklochko.simple_jbdd.tests.factories.TestCommandFactory
import spock.lang.Narrative
import spock.lang.Specification
import spock.lang.Subject
import spock.lang.Title

@Narrative("""The runner must return a result of test, so
those tests check if passed and failed test generate the right result.
""")
@Title("Unit tests for SimpleTestRunner")
class SimpleTestRunnerSpec extends Specification {
    def "The test with then has been passed."() {
        given: "I have a testCommand"
        def test = Stub(SimpleTestCommand)
        test.test() >>> [true]

        and: "I have a test runner"
        @Subject
        def runner = new SimpleTestRunner(test)

        when: "The testCommand has been run"
        def result = runner.run()

        then: "The result is true"
        result
    }

    def "The test with then that has not been passed."() {
        given: "I have a failed testCommand"
        def test = Stub(SimpleTestCommand)
        test.test() >> { assert 1 == 0 }

        and: "I have a test runner"
        @Subject
        def runner = new SimpleTestRunner(test)

        when: "The testCommand has been run"
        def result = runner.run()

        then: "The result is false"
        !result
    }
}

