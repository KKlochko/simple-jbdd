package space.kklochko.simple_jbdd.tests.runners

import space.kklochko.simple_jbdd.test_examples.tests.SimpleEmptyTest
import space.kklochko.simple_jbdd.test_examples.tests.SimpleFailedThenTest
import space.kklochko.simple_jbdd.test_examples.tests.SimpleGivenGivenWhenWhenThenThenTest
import space.kklochko.simple_jbdd.test_examples.tests.SimpleGivenWhenThenTest
import space.kklochko.simple_jbdd.test_examples.tests.SimpleThenTest
import space.kklochko.simple_jbdd.test_examples.tests.SimpleThenTestWithoutTitle
import space.kklochko.simple_jbdd.tests.factories.TestCommandFactory
import spock.lang.Narrative
import spock.lang.Specification
import spock.lang.Subject
import spock.lang.Title

@Narrative("""The runner must return a result of test, so
those tests check if passed and failed test generate the right result.
""")
@Title("Integrated tests for SimpleTestRunner with TestCommandFactory and test examples")
class IntegratedSimpleTestRunnerSpec extends Specification {
    def "The real test has been run."() {
        given: "I have a factory"
        def factory = new TestCommandFactory();

        and: "I have a testCommand created from a test object"
        def test = factory.create(testObject)

        and: "I have a test runner"
        @Subject
        def runner = new SimpleTestRunner(test)

        when: "The testCommand has been run"
        def result = runner.run()

        then: "Checking that the test result is expected"
        result == expectedResult

        where: "Possible variants of tests"
        testObject                                 || expectedResult
        new SimpleThenTest()                       || true
        new SimpleGivenWhenThenTest()              || true
        new SimpleThenTestWithoutTitle()           || true
        new SimpleFailedThenTest()                 || false
        new SimpleGivenGivenWhenWhenThenThenTest() || true
        new SimpleEmptyTest()                      || false
    }
}
