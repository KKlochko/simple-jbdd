package space.kklochko.simple_jbdd.tests.reports.fmt

import space.kklochko.simple_jbdd.test_examples.tests.SimpleFailedThenTest
import space.kklochko.simple_jbdd.test_examples.tests.SimpleGivenWhenThenTest
import space.kklochko.simple_jbdd.test_examples.tests.SimpleThenTest
import space.kklochko.simple_jbdd.test_examples.tests.SimpleThenTestWithoutTitle
import space.kklochko.simple_jbdd.tests.commands.AbstractTestCommand
import space.kklochko.simple_jbdd.tests.commands.decorators.BlockDecorator
import space.kklochko.simple_jbdd.tests.factories.meta.TestCommandMetaDataFactory
import spock.lang.Narrative
import spock.lang.Specification
import spock.lang.Subject
import spock.lang.Title

@Narrative("""The formatter must generate a format for the test command metadata, so
those tests check if the formatter generate it right.
""")
@Title("Unit tests for SimpleTestReportFormatter")
class SimpleTestReportFormatterSpec extends Specification {
    def "If the map of methods have one test block."() {
        given: "I have a metaData"
        def metaData = [
            new AbstractMap.SimpleEntry<>("Title", "A simple test"),
            new AbstractMap.SimpleEntry<>("Then", "Then block")
        ]

        and: "I have the formatter object"
        @Subject
        def formatter = new SimpleTestReportFormatter(metaData, isPassed)

        when: "Generate the format"
        def format = formatter.format()

        then: "The format must be the same"
        expectedFormat == format

        where: "Possible variants of tests"
        isPassed || expectedFormat
        true     || "✓ A simple test\n\tThen: Then block\n\n"
        false    || "⨯ A simple test\n\tThen: Then block\n\n"
    }

    def "If the map of methods have GivenWhenThen blocks."() {
        given: "I have a metaData"
        def metaData = [
            new AbstractMap.SimpleEntry<>("Title", "A simple test"),
            new AbstractMap.SimpleEntry<>("Given", "Given block"),
            new AbstractMap.SimpleEntry<>("When", "When block"),
            new AbstractMap.SimpleEntry<>("Then", "Then block")
        ]

        and: "I have the formatter object"
        @Subject
        def formatter = new SimpleTestReportFormatter(metaData, isPassed)

        when: "Generate the format"
        def format = formatter.format()

        then: "The format must be the same"
        expectedFormat == format

        where: "Possible variants of tests"
        isPassed || expectedFormat
        true     || "✓ A simple test\n\tGiven: Given block\n\tWhen: When block\n\tThen: Then block\n\n"
        false    || "⨯ A simple test\n\tGiven: Given block\n\tWhen: When block\n\tThen: Then block\n\n"
    }
}

