package space.kklochko.simple_jbdd.tests.reports

import space.kklochko.simple_jbdd.tests.factories.meta.AbstractTestCommandMetaDataFactory
import space.kklochko.simple_jbdd.tests.reports.fmt.AbstractTestReportFormatter
import space.kklochko.simple_jbdd.tests.reports.fmt.SimpleTestReportFormatter
import spock.lang.Narrative
import spock.lang.Specification
import spock.lang.Subject
import spock.lang.Title

@Narrative("""The reporter must generate a report in the same format, so
those tests check if the reporter generate it right.
""")
@Title("Unit tests for CLIReporter")
class CLIReporterSpec extends Specification {
    def "Function, getFailedTest, returns the data of failed tests."() {
        given: "I have a reporter"
        @Subject
        def reporter = new CLIReporter(null, null)

        and: "I have a testData"
        def passedTestPair = new AbstractMap.SimpleEntry<>("Passed", true)
        def failedTestPair = new AbstractMap.SimpleEntry<>("Failed", false)
        def testData = Collections.nCopies(countPassed, passedTestPair) + Collections.nCopies(countFailed, failedTestPair)

        when: "Generate the summary format"
        def failedTestData = reporter.getFailedTests(testData)

        then: "The format must be the same"
        failedTestData.every({ it.getKey() == "Failed" && it.getValue() == false})
        expectedFailedCount == failedTestData.size()

        where: "Possible variants of tests"
        countPassed | countFailed || expectedFailedCount
        5           | 10          || 10
    }

    def "Count function returns the count of passed or failed tests."() {
        given: "I have a reporter"
        @Subject
        def reporter = new CLIReporter(null, null)

        and: "I have a testData"
        def passedTestPair = new AbstractMap.SimpleEntry<>("Passed", true)
        def failedTestPair = new AbstractMap.SimpleEntry<>("Failed", false)
        def testData = Collections.nCopies(countPassed, passedTestPair) + Collections.nCopies(countFailed, failedTestPair)

        when: "Generate the summary format"
        def count = reporter.count(testData, isPassed)

        then: "The format must be the same"
        expectedCount == count

        where: "Possible variants of tests"
        countPassed | countFailed | isPassed || expectedCount
        10          | 20          | true     || 10
        10          | 20          | false    || 20
    }

    def "Summary format."() {
        given: "I have a reporter"
        @Subject
        def reporter = new CLIReporter(null, null)

        when: "Generate the summary format"
        def format = reporter.summary(passed, failed)

        then: "The format must be the same"
        expectedFormat == format

        where: "Possible variants of tests"
        passed | failed || expectedFormat
        10     | 20     || "Ran 30 tests.\nPassed: 10 (33.33%)\nFailed: 20 (66.67%)\n"
        10     | 10     || "Ran 20 tests.\nPassed: 10 (50.00%)\nFailed: 10 (50.00%)\n"
    }
}

