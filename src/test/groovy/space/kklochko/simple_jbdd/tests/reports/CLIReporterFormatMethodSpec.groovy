package space.kklochko.simple_jbdd.tests.reports

import space.kklochko.simple_jbdd.tests.factories.meta.AbstractTestCommandMetaDataFactory
import space.kklochko.simple_jbdd.tests.reports.fmt.AbstractTestReportFormatter
import space.kklochko.simple_jbdd.tests.reports.fmt.SimpleTestReportFormatter
import spock.lang.Narrative
import spock.lang.Specification
import spock.lang.Subject
import spock.lang.Title

@Narrative("""The reporter must generate a report in the same format, so
those tests check if the reporter generate it right.
""")
@Title("Unit tests for CLIReporter.format()")
class CLIReporterFormatMethodSpec extends Specification {
    private metaData

    def setup() {
        def factory = Mock(AbstractTestCommandMetaDataFactory) {
            create() >> [
                    new AbstractMap.SimpleEntry<>("Title", "A simple test"),
                    new AbstractMap.SimpleEntry<>("Then", "Then block")
            ]
        }

        def metaData = [
                new AbstractMap.SimpleEntry<>(factory, true),
                new AbstractMap.SimpleEntry<>(factory, false)
        ]

        this.metaData = metaData
    }

    def "Format function use formatter twice."() {
        given: "I have a formatter"
        def formatter = Mock(AbstractTestReportFormatter)

        and: "I have a reporter"
        @Subject
        def reporter = new CLIReporter(metaData, formatter)

        when: "Generate the summary format"
        def formattedTestData = reporter.format()

        then: "The methods must called 2 times"
        2 * formatter.format()
        2 * formatter.setPassed(_)
        2 * formatter.setTestMetaData(_)

        and: "The data must have 2 elements"
        2 == formattedTestData.size()
    }

    def "Format function returns the formatted data."() {
        given: "I have a formatter"
        def formatter = new SimpleTestReportFormatter()

        and: "I have a reporter"
        @Subject
        def reporter = new CLIReporter(metaData, formatter)

        when: "Generate the summary format"
        def formattedTestData = reporter.format()

        then: "The format must have not null values"
        formattedTestData.every({ it.getKey() != null })

        and: "The data must have 2 elements"
        2 == formattedTestData.size()
    }
}

