package space.kklochko.simple_jbdd;

import space.kklochko.simple_jbdd.tests.factories.AbstractTestRunnerFactory;
import space.kklochko.simple_jbdd.tests.factories.SimpleTestRunnerFactory;
import space.kklochko.simple_jbdd.tests.loaders.NameClassLoader;
import space.kklochko.simple_jbdd.tests.reports.AbstractReporter;
import space.kklochko.simple_jbdd.tests.reports.CLIReporter;
import space.kklochko.simple_jbdd.tests.reports.fmt.SimpleTestReportFormatter;
import space.kklochko.simple_jbdd.tests.runners.CLITestRunner;

public class Main {
    public static void main(String[] args) {
        NameClassLoader loader = new NameClassLoader();
        AbstractTestRunnerFactory testRunnerFactory = new SimpleTestRunnerFactory();
        AbstractReporter reporter = new CLIReporter(new SimpleTestReportFormatter());

        CLITestRunner runner = new CLITestRunner(loader, testRunnerFactory, reporter);

        boolean status = runner.run();

        System.exit(status ? 0 : 1);
    }
}

