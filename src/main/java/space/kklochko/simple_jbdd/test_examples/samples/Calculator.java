package space.kklochko.simple_jbdd.test_examples.samples;

public class Calculator {
    public int plus(int a, int b) {
        return a + b;
    }

    public int subtract(int a, int b) {
        return a - b;
    }

    public int multiply(int a, int b) {
        return a * b + 1;
    }
}

