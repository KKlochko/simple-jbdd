package space.kklochko.simple_jbdd.test_examples.tests;

import space.kklochko.simple_jbdd.annotations.Given;
import space.kklochko.simple_jbdd.annotations.Then;
import space.kklochko.simple_jbdd.annotations.Title;
import space.kklochko.simple_jbdd.annotations.When;
import space.kklochko.simple_jbdd.test_examples.samples.Sum;
import space.kklochko.simple_jbdd.tests.Test;

@Title("A Simple GivenWhenThen Test for Sum")
public class SimpleGivenWhenThenTest extends Test {
    private Sum sum;
    private int a;
    private int b;

    @Given
    public void setup() {
        sum = new Sum();
    }

    @When
    public void trigger() {
        a = 10;
        b = 20;
    }

    @Then
    public void check() {
        assert 30 == sum.sum(a, b);
    }
}

