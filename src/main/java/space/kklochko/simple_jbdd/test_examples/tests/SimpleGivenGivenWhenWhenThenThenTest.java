package space.kklochko.simple_jbdd.test_examples.tests;

import space.kklochko.simple_jbdd.annotations.Given;
import space.kklochko.simple_jbdd.annotations.Then;
import space.kklochko.simple_jbdd.annotations.Title;
import space.kklochko.simple_jbdd.annotations.When;
import space.kklochko.simple_jbdd.test_examples.samples.Sum;
import space.kklochko.simple_jbdd.tests.Test;

@Title("A Simple GivenGivenWhenWhenThenThen Test for Sum")
public class SimpleGivenGivenWhenWhenThenThenTest extends Test {
    private Sum sum;
    private int a;
    private int b;
    private int c;
    private int resultAB;
    private int resultCB;

    @Given("Create the Sum object")
    public void setup() {
        sum = new Sum();
    }

    @Given("Setup the arguments")
    public void setupArgs() {
        a = 10;
        b = 20;
        c = 30;
    }

    @When("Call the .sum(10, 20)")
    public void triggerAB() {
        resultAB = sum.sum(a, b);
    }

    @When("Call the .sum(30, 20)")
    public void triggerCB() {
        resultCB = sum.sum(c, b);
    }

    @Then("10 + 20 is 30")
    public void checkAB() {
        assert 30 == resultAB;
    }

    @Then("30 + 20 is 50")
    public void checkCB() {
        assert 50 == resultCB;
    }
}

