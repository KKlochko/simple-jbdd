package space.kklochko.simple_jbdd.test_examples.tests;

import space.kklochko.simple_jbdd.annotations.Given;
import space.kklochko.simple_jbdd.annotations.Then;
import space.kklochko.simple_jbdd.annotations.Title;
import space.kklochko.simple_jbdd.annotations.When;
import space.kklochko.simple_jbdd.test_examples.samples.Calculator;
import space.kklochko.simple_jbdd.tests.Test;

@Title("3*3 must be 9")
public class CalculatorMultiplyTest extends Test {
    Calculator calculator;
    int a;
    int b;
    int result;

    @Given("Set up the calculator")
    public void given() {
        calculator = new Calculator();
    }

    @Given("Set up the arguments")
    public void setupArguments() {
        a = 3;
        b = 3;
    }

    @When("Invoke the multiply method")
    public void when() {
        result = calculator.multiply(a, b);
    }

    @Then("3*3 must be 9")
    public void then() {
        assert result == 9;
    }
}

