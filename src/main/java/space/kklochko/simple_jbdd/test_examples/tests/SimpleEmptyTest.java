package space.kklochko.simple_jbdd.test_examples.tests;

import space.kklochko.simple_jbdd.annotations.Title;
import space.kklochko.simple_jbdd.tests.Test;

@Title("A Simple Empty Test for Sum")
public class SimpleEmptyTest extends Test {
    public void check() {
        assert 30 == 10 + 20 + 1;
    }
}

