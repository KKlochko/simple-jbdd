package space.kklochko.simple_jbdd.test_examples.tests;

import space.kklochko.simple_jbdd.annotations.Given;
import space.kklochko.simple_jbdd.annotations.Then;
import space.kklochko.simple_jbdd.annotations.Title;
import space.kklochko.simple_jbdd.annotations.When;
import space.kklochko.simple_jbdd.test_examples.samples.Calculator;
import space.kklochko.simple_jbdd.tests.Test;

@Title("Test with a method that return boolean")
public class HasNonVoidMethodTest extends Test {
    Calculator calculator;
    int a;
    int b;
    int result;

    @Given("Set up the calculator in a private method")
    public void given() {
        calculator = new Calculator();
    }

    @Given("Set up the arguments")
    public void setupArguments() {
        this.a = 4;
        this.b = 2;
    }

    @When("Invoke the subtract method")
    public void when() {
        result = calculator.subtract(a, b);
    }

    @Then("4-2 must be 2 and return result")
    public boolean then() {
        assert result == 2;
        return result != 2;
    }
}

