package space.kklochko.simple_jbdd.test_examples.tests;

import space.kklochko.simple_jbdd.annotations.Given;
import space.kklochko.simple_jbdd.annotations.Then;
import space.kklochko.simple_jbdd.annotations.Title;
import space.kklochko.simple_jbdd.annotations.When;
import space.kklochko.simple_jbdd.test_examples.samples.Calculator;
import space.kklochko.simple_jbdd.tests.Test;

@Title("4-2 must be 2")
public class CalculatorSubtractTest extends Test {
    Calculator calculator;
    int a;
    int b;
    int result;

    @Given("Set up the calculator")
    public void given() {
        calculator = new Calculator();
    }

    @Given("Set up the arguments")
    public void setupArguments() {
        a = 4;
        b = 2;
    }

    @When("Invoke the subtract method")
    public void when() {
        result = calculator.subtract(a, b);
    }

    @Then("4-2 must be 2")
    public void then() {
        assert result == 2;
    }
}

