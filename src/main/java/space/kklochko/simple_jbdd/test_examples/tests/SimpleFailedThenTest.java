package space.kklochko.simple_jbdd.test_examples.tests;

import space.kklochko.simple_jbdd.annotations.Then;
import space.kklochko.simple_jbdd.annotations.Title;
import space.kklochko.simple_jbdd.tests.Test;


@Title("A Simple Failed Then Test for Sum")
public class SimpleFailedThenTest extends Test {
    @Then
    public void check() {
        assert 30 == 10 + 20 + 1;
    }
}

