package space.kklochko.simple_jbdd.test_examples.tests;

import space.kklochko.simple_jbdd.annotations.Then;
import space.kklochko.simple_jbdd.annotations.Title;
import space.kklochko.simple_jbdd.tests.Test;

@Title("Simple test")
public class SimpleThenTest extends Test {
    @Then("The sum of 10 and 20 is 30.")
    public void check() {
        assert 30 == 10 + 20;
    }
}

