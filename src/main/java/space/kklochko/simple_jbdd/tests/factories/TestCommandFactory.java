//////////////////////////////////////////////////////////////////////////////////
// Copyright (C) 2023 Kostiantyn Klochko <kklochko@protonmail.com>              //
//                                                                              //
// This file is part of simple-jbdd.                                            //
//                                                                              //
// simple-jbdd is free software: you can redistribute it and/or modify it under //
// the terms of the GNU Lesser General Public License as published by the Free  //
// Software Foundation, either version 3 of the License, or (at your option)    //
// any later version.                                                           //
//                                                                              //
// simple-jbdd is distributed in the hope that it will be useful, but WITHOUT   //
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or        //
// FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License  //
// for more details.                                                            //
//                                                                              //
// You should have received a copy of the GNU Lesser General Public License     //
// along with simple-jbdd. If not, see <https://www.gnu.org/licenses/>.         //
//////////////////////////////////////////////////////////////////////////////////

package space.kklochko.simple_jbdd.tests.factories;

import space.kklochko.simple_jbdd.annotations.Given;
import space.kklochko.simple_jbdd.annotations.Then;
import space.kklochko.simple_jbdd.annotations.Title;
import space.kklochko.simple_jbdd.annotations.When;
import space.kklochko.simple_jbdd.tests.Test;
import space.kklochko.simple_jbdd.tests.commands.AbstractTestCommand;
import space.kklochko.simple_jbdd.tests.commands.SimpleTestCommand;
import space.kklochko.simple_jbdd.tests.commands.decorators.BlockDecorator;
import space.kklochko.simple_jbdd.tests.commands.decorators.ErrorDecorator;
import space.kklochko.simple_jbdd.tests.factories.validators.*;

import java.lang.annotation.Annotation;
import java.lang.reflect.Method;
import java.util.*;

public class TestCommandFactory {
    public AbstractTestCommand create(Test input) {
	    Class<?> aClass = input.getClass();
	    Map<String, ArrayList<Method>> test_methods = this.getTestMethods(aClass);

        AbstractTestCommand<Test> aTestCommand = createTest(aClass, input);

        ArrayList<Validator> validators = new ArrayList<>();
        validators.add(new EmptyValidator());
        validators.add(new HasMethodsWithArgumentsValidator());
        validators.add(new HasNonVoidMethodsValidator());
        validators.add(new HasPrivateMethodsValidator());

        for(var validator : validators) {
            if(validator.validate(test_methods)) {
                String name = validator.getMessage();
                return new ErrorDecorator(name, aTestCommand, validator.getErrorType());
            }
        }

        if(test_methods.containsKey("Then")) {
            ArrayList<Method> thens = getMethodsInReverseOrder(test_methods, "Then");
            for(Method then : thens) {
                String label = then.getAnnotation(Then.class).value();
                aTestCommand = new BlockDecorator<Test>(label, aTestCommand, then, "Then");
            }
        }

        if(test_methods.containsKey("When")) {
            ArrayList<Method> whens = getMethodsInReverseOrder(test_methods, "When");
            for(Method when : whens) {
                String label = when.getAnnotation(When.class).value();
                aTestCommand = new BlockDecorator<Test>(label, aTestCommand, when, "When");
            }
        }

        if(test_methods.containsKey("Given")) {
            ArrayList<Method> givens = getMethodsInReverseOrder(test_methods, "Given");
            for(Method given : givens) {
                String label = given.getAnnotation(Given.class).value();
                aTestCommand = new BlockDecorator<Test>(label, aTestCommand, given, "Given");
            }
        }

        return aTestCommand;
    }

    private ArrayList<Method> getMethodsInReverseOrder(Map<String, ArrayList<Method>> test_methods, String key) {
        ArrayList<Method> methods = test_methods.get(key);
        Collections.reverse(methods);
        return methods;
    }

    protected AbstractTestCommand<Test> createTest(Class<?> aClass, Test input) {
        Title aTitle = aClass.getAnnotation(Title.class);
        String title = aClass.getName();

        if(aTitle != null)
            title = aTitle.value();

        return new SimpleTestCommand<Test>(title, input);
    }

    protected <T extends Test> Map<String, ArrayList<Method>> getTestMethods(Class<?> aClass) {
        Map<String, ArrayList<Method>> test_methods = new TreeMap<>();

        for(Method method : aClass.getDeclaredMethods()) {
            Annotation[] annotations = method.getAnnotations();

            if (annotations.length == 0)
                continue;

            if(method.isAnnotationPresent(Given.class))
                addMethodTo(test_methods, "Given", method);

            if(method.isAnnotationPresent(When.class))
                addMethodTo(test_methods, "When", method);

            if(method.isAnnotationPresent(Then.class))
                addMethodTo(test_methods, "Then", method);
        }

        return test_methods;
    }

    private void addMethodTo(Map<String, ArrayList<Method>> test_methods, String key, Method method) {
        ArrayList<Method> methods = test_methods.getOrDefault(key, new ArrayList<>());
        methods.add(method);
        test_methods.put(key, methods);
    }
}
