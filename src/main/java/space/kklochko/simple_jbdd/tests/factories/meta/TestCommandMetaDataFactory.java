//////////////////////////////////////////////////////////////////////////////////
// Copyright (C) 2023 Kostiantyn Klochko <kklochko@protonmail.com>              //
//                                                                              //
// This file is part of simple-jbdd.                                            //
//                                                                              //
// simple-jbdd is free software: you can redistribute it and/or modify it under //
// the terms of the GNU Lesser General Public License as published by the Free  //
// Software Foundation, either version 3 of the License, or (at your option)    //
// any later version.                                                           //
//                                                                              //
// simple-jbdd is distributed in the hope that it will be useful, but WITHOUT   //
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or        //
// FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License  //
// for more details.                                                            //
//                                                                              //
// You should have received a copy of the GNU Lesser General Public License     //
// along with simple-jbdd. If not, see <https://www.gnu.org/licenses/>.         //
//////////////////////////////////////////////////////////////////////////////////

package space.kklochko.simple_jbdd.tests.factories.meta;

import space.kklochko.simple_jbdd.tests.commands.AbstractTestCommand;
import space.kklochko.simple_jbdd.tests.commands.decorators.AbstractDecorator;

import java.util.AbstractMap;
import java.util.ArrayList;

public class TestCommandMetaDataFactory extends AbstractTestCommandMetaDataFactory {
    private AbstractTestCommand command;

    public TestCommandMetaDataFactory(AbstractTestCommand command) {
        this.command = command;
    }

    public ArrayList<AbstractMap.SimpleEntry<String, String>> create() {
        ArrayList<AbstractMap.SimpleEntry<String, String>> blocks = new ArrayList<>();
        AbstractTestCommand currentCommand = command;
        Class<?> aClass = command.getClass();

        while(AbstractDecorator.class.isAssignableFrom(aClass)) {
            AbstractDecorator block = (AbstractDecorator) currentCommand;
            blocks.add(new AbstractMap.SimpleEntry<>(block.getType(), block.getLabel()));

            currentCommand = block.getCommand();
            aClass = currentCommand.getClass();
        }

        blocks.add(0, (new AbstractMap.SimpleEntry<>(currentCommand.getType(), currentCommand.getLabel())));

        return blocks;
    }
}

