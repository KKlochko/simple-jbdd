//////////////////////////////////////////////////////////////////////////////////
// Copyright (C) 2023 Kostiantyn Klochko <kklochko@protonmail.com>              //
//                                                                              //
// This file is part of simple-jbdd.                                            //
//                                                                              //
// simple-jbdd is free software: you can redistribute it and/or modify it under //
// the terms of the GNU Lesser General Public License as published by the Free  //
// Software Foundation, either version 3 of the License, or (at your option)    //
// any later version.                                                           //
//                                                                              //
// simple-jbdd is distributed in the hope that it will be useful, but WITHOUT   //
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or        //
// FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License  //
// for more details.                                                            //
//                                                                              //
// You should have received a copy of the GNU Lesser General Public License     //
// along with simple-jbdd. If not, see <https://www.gnu.org/licenses/>.         //
//////////////////////////////////////////////////////////////////////////////////

package space.kklochko.simple_jbdd.tests.factories;

import space.kklochko.simple_jbdd.tests.Test;
import space.kklochko.simple_jbdd.tests.commands.AbstractTestCommand;

import java.util.*;

public class TestCommandQueueFactory {
    private ArrayList<Test> tests;
    private TestCommandFactory factory;

    public TestCommandQueueFactory(ArrayList<Test> tests) {
        setTests(tests);
        setFactory(new TestCommandFactory());
    }

    public TestCommandQueueFactory(ArrayList<Test> tests, TestCommandFactory factory) {
        setTests(tests);
        setFactory(factory);
    }

    public ArrayList<AbstractTestCommand<Test>> create() {
        ArrayList<AbstractTestCommand<Test>> commands = new ArrayList<AbstractTestCommand<Test>>();

        for(Test test : getTests()) {
            AbstractTestCommand<Test> command = getFactory().create(test);
            commands.add(command);
        }

        return commands;
    }

    public ArrayList<Test> getTests() {
        return tests;
    }

    public void setTests(ArrayList<Test> tests) {
        this.tests = tests;
    }

    public TestCommandFactory getFactory() {
        return factory;
    }

    public void setFactory(TestCommandFactory factory) {
        this.factory = factory;
    }
}

