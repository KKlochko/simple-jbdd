//////////////////////////////////////////////////////////////////////////////////
// Copyright (C) 2023 Kostiantyn Klochko <kklochko@protonmail.com>              //
//                                                                              //
// This file is part of simple-jbdd.                                            //
//                                                                              //
// simple-jbdd is free software: you can redistribute it and/or modify it under //
// the terms of the GNU Lesser General Public License as published by the Free  //
// Software Foundation, either version 3 of the License, or (at your option)    //
// any later version.                                                           //
//                                                                              //
// simple-jbdd is distributed in the hope that it will be useful, but WITHOUT   //
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or        //
// FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License  //
// for more details.                                                            //
//                                                                              //
// You should have received a copy of the GNU Lesser General Public License     //
// along with simple-jbdd. If not, see <https://www.gnu.org/licenses/>.         //
//////////////////////////////////////////////////////////////////////////////////

package space.kklochko.simple_jbdd.tests.factories.validators;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Map;
import java.util.stream.Collectors;

abstract public class MethodValidator extends Validator{
    protected boolean isInvalid;

    protected ArrayList<Method> invalidMethods;

    protected String getMethodListAsString() {
        return invalidMethods.stream()
                .map(m -> m.getName())
                .collect(Collectors.joining(", "));
    }

    abstract protected boolean isMethodInvalid(Method method);

    @Override
    public boolean validate(Map<String, ArrayList<Method>> methods) {
        isInvalid = false;
        invalidMethods = new ArrayList<>();

        for(var key : methods.keySet()) {
            for(var method : methods.get(key)) {
                if(isMethodInvalid(method)) {
                    isInvalid = true;
                    invalidMethods.add(method);
                }
            }
        }

        return isInvalid;
    }

    @Override
    public String getErrorType() {
        if(!isInvalid)
            return "ok";

        return "Error";
    }

    abstract public String getMessage();
}

