//////////////////////////////////////////////////////////////////////////////////
// Copyright (C) 2023 Kostiantyn Klochko <kklochko@protonmail.com>              //
//                                                                              //
// This file is part of simple-jbdd.                                            //
//                                                                              //
// simple-jbdd is free software: you can redistribute it and/or modify it under //
// the terms of the GNU Lesser General Public License as published by the Free  //
// Software Foundation, either version 3 of the License, or (at your option)    //
// any later version.                                                           //
//                                                                              //
// simple-jbdd is distributed in the hope that it will be useful, but WITHOUT   //
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or        //
// FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License  //
// for more details.                                                            //
//                                                                              //
// You should have received a copy of the GNU Lesser General Public License     //
// along with simple-jbdd. If not, see <https://www.gnu.org/licenses/>.         //
//////////////////////////////////////////////////////////////////////////////////

package space.kklochko.simple_jbdd.tests.factories.validators;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Map;

public class EmptyValidator extends Validator {
    private boolean isInvalid;

    @Override
    public boolean validate(Map<String, ArrayList<Method>> methods) {
        isInvalid = (methods.size() == 0);
        return isInvalid;
    }

    @Override
    public String getErrorType() {
        if(!isInvalid)
            return "ok";

        return "Error";
    }

    @Override
    public String getMessage() {
        if(!isInvalid)
            return "ok";

        return "ERROR!!! No blocks!!!";
    }
}

