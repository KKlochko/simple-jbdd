//////////////////////////////////////////////////////////////////////////////////
// Copyright (C) 2023 Kostiantyn Klochko <kklochko@protonmail.com>              //
//                                                                              //
// This file is part of simple-jbdd.                                            //
//                                                                              //
// simple-jbdd is free software: you can redistribute it and/or modify it under //
// the terms of the GNU Lesser General Public License as published by the Free  //
// Software Foundation, either version 3 of the License, or (at your option)    //
// any later version.                                                           //
//                                                                              //
// simple-jbdd is distributed in the hope that it will be useful, but WITHOUT   //
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or        //
// FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License  //
// for more details.                                                            //
//                                                                              //
// You should have received a copy of the GNU Lesser General Public License     //
// along with simple-jbdd. If not, see <https://www.gnu.org/licenses/>.         //
//////////////////////////////////////////////////////////////////////////////////

package space.kklochko.simple_jbdd.tests.reports;

import space.kklochko.simple_jbdd.tests.factories.meta.AbstractTestCommandMetaDataFactory;
import space.kklochko.simple_jbdd.tests.reports.fmt.AbstractTestReportFormatter;

import java.util.AbstractMap;
import java.util.ArrayList;
import org.apache.commons.lang3.StringUtils;

public class CLIReporter extends AbstractReporter {
    final int terminalWidth = 80;

    public CLIReporter(AbstractTestReportFormatter formatter) {
        super(null, formatter);
    }

    public CLIReporter(ArrayList<AbstractMap.SimpleEntry<AbstractTestCommandMetaDataFactory, Boolean>> metadata, AbstractTestReportFormatter formatter) {
        super(metadata, formatter);
    }

    public void report() {
        ArrayList<AbstractMap.SimpleEntry<String, Boolean>> testData = format();
        ArrayList<AbstractMap.SimpleEntry<String, Boolean>> failedTestData = getFailedTests(testData);
        int passed = count(testData, true);
        int failed = count(testData, false);

        System.out.printf("%s\n\n", center(" Tests "));
        printTests(testData);

        if(failed != 0) {
            System.out.printf("%s\n\n", center(" Errors "));
            printTests(failedTestData);
        }

        System.out.printf("%s\n", center(" Summary "));
        System.out.print(summary(passed, failed));
    }

    public ArrayList<AbstractMap.SimpleEntry<String, Boolean>> format() {
        ArrayList<AbstractMap.SimpleEntry<String, Boolean>> data = new ArrayList<>();

        for(var pair : getMetadata()) {
            boolean isPassed = pair.getValue();

            getFormatter().setTestMetaData(pair.getKey().create());
            getFormatter().setPassed(isPassed);

            data.add(new AbstractMap.SimpleEntry<>(getFormatter().format(), isPassed));
        }

        return data;
    }

    public ArrayList<AbstractMap.SimpleEntry<String, Boolean>> getFailedTests(ArrayList<AbstractMap.SimpleEntry<String, Boolean>> testData) {
        ArrayList<AbstractMap.SimpleEntry<String, Boolean>> failed = new ArrayList<>();

        for(var pair : testData) {
            boolean isPassed = pair.getValue();

            if(isPassed) continue;

            failed.add(pair);
        }

        return failed;
    }

    public int count(ArrayList<AbstractMap.SimpleEntry<String, Boolean>> testData, boolean isPassedStatus) {
        int count = 0;

        for(var pair : testData) {
            boolean isPassed = pair.getValue();

            if(isPassed == isPassedStatus)
                count++;
        }

        return count;
    }

    public void printTests(ArrayList<AbstractMap.SimpleEntry<String, Boolean>> testData) {
        for(var pair : testData) {
            System.out.print(pair.getKey());
        }
    }

    public String center(String content) {
        return StringUtils.center(content, 80, '=');
    }

    public String summary(int passed, int failed) {
        int all = passed + failed;
        double passedPercent = (double) passed / (double) all * 100.0;
        double failedPercent = (double) failed / (double) all * 100.0;

        return String.format("Ran %d tests.\nPassed: %d (%.2f%%)\nFailed: %d (%.2f%%)\n", all, passed, passedPercent, failed, failedPercent);
    }
}

