//////////////////////////////////////////////////////////////////////////////////
// Copyright (C) 2023 Kostiantyn Klochko <kklochko@protonmail.com>              //
//                                                                              //
// This file is part of simple-jbdd.                                            //
//                                                                              //
// simple-jbdd is free software: you can redistribute it and/or modify it under //
// the terms of the GNU Lesser General Public License as published by the Free  //
// Software Foundation, either version 3 of the License, or (at your option)    //
// any later version.                                                           //
//                                                                              //
// simple-jbdd is distributed in the hope that it will be useful, but WITHOUT   //
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or        //
// FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License  //
// for more details.                                                            //
//                                                                              //
// You should have received a copy of the GNU Lesser General Public License     //
// along with simple-jbdd. If not, see <https://www.gnu.org/licenses/>.         //
//////////////////////////////////////////////////////////////////////////////////

package space.kklochko.simple_jbdd.tests.reports.fmt;

import space.kklochko.simple_jbdd.tests.Test;

import java.util.AbstractMap;
import java.util.ArrayList;

public class SimpleTestReportFormatter extends AbstractTestReportFormatter {
    final private String passedIcon = "✓";
    final private String failedIcon = "⨯";

    public SimpleTestReportFormatter() {
        super(null, false);
    }

    public SimpleTestReportFormatter(ArrayList<AbstractMap.SimpleEntry<String, String>> testMetaData, boolean isPassed) {
        super(testMetaData, isPassed);
    }

    public String format() {
        String status = (isPassed()) ? getPassedIcon() : getFailedIcon();
        String title = getTestMetaData().get(0).getValue();

        String result = String.format("%s %s\n", status, title);

        for(int i=1; i<getTestMetaData().size(); ++i) {
            AbstractMap.SimpleEntry<String, String> pair = getTestMetaData().get(i);
            result += String.format("\t%s: %s\n", pair.getKey(), pair.getValue());
        }

        return result + '\n';
    }

    public String getPassedIcon() {
        return passedIcon;
    }

    public String getFailedIcon() {
        return failedIcon;
    }
}

