//////////////////////////////////////////////////////////////////////////////////
// Copyright (C) 2023 Kostiantyn Klochko <kklochko@protonmail.com>              //
//                                                                              //
// This file is part of simple-jbdd.                                            //
//                                                                              //
// simple-jbdd is free software: you can redistribute it and/or modify it under //
// the terms of the GNU Lesser General Public License as published by the Free  //
// Software Foundation, either version 3 of the License, or (at your option)    //
// any later version.                                                           //
//                                                                              //
// simple-jbdd is distributed in the hope that it will be useful, but WITHOUT   //
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or        //
// FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License  //
// for more details.                                                            //
//                                                                              //
// You should have received a copy of the GNU Lesser General Public License     //
// along with simple-jbdd. If not, see <https://www.gnu.org/licenses/>.         //
//////////////////////////////////////////////////////////////////////////////////

package space.kklochko.simple_jbdd.tests.reports;

import space.kklochko.simple_jbdd.tests.factories.meta.AbstractTestCommandMetaDataFactory;
import space.kklochko.simple_jbdd.tests.reports.fmt.AbstractTestReportFormatter;

import java.util.AbstractMap;
import java.util.ArrayList;

public abstract class AbstractReporter {
    ArrayList<AbstractMap.SimpleEntry<AbstractTestCommandMetaDataFactory, Boolean>> Metadata;
    AbstractTestReportFormatter formatter;

    public AbstractReporter(ArrayList<AbstractMap.SimpleEntry<AbstractTestCommandMetaDataFactory, Boolean>> metadata, AbstractTestReportFormatter formatter) {
        setMetadata(metadata);
        setFormatter(formatter);
    }

    abstract public void report();

    public ArrayList<AbstractMap.SimpleEntry<AbstractTestCommandMetaDataFactory, Boolean>> getMetadata() {
        return Metadata;
    }

    public void setMetadata(ArrayList<AbstractMap.SimpleEntry<AbstractTestCommandMetaDataFactory, Boolean>> metadata) {
        Metadata = metadata;
    }

    public void addMetadata(AbstractTestCommandMetaDataFactory testMetaData, boolean isPassed) {
        getMetadata().add(new AbstractMap.SimpleEntry<>(testMetaData, isPassed));
    }

    public AbstractTestReportFormatter getFormatter() {
        return formatter;
    }

    public void setFormatter(AbstractTestReportFormatter formatter) {
        this.formatter = formatter;
    }
}

