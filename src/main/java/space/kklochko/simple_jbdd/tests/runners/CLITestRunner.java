//////////////////////////////////////////////////////////////////////////////////
// Copyright (C) 2023 Kostiantyn Klochko <kklochko@protonmail.com>              //
//                                                                              //
// This file is part of simple-jbdd.                                            //
//                                                                              //
// simple-jbdd is free software: you can redistribute it and/or modify it under //
// the terms of the GNU Lesser General Public License as published by the Free  //
// Software Foundation, either version 3 of the License, or (at your option)    //
// any later version.                                                           //
//                                                                              //
// simple-jbdd is distributed in the hope that it will be useful, but WITHOUT   //
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or        //
// FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License  //
// for more details.                                                            //
//                                                                              //
// You should have received a copy of the GNU Lesser General Public License     //
// along with simple-jbdd. If not, see <https://www.gnu.org/licenses/>.         //
//////////////////////////////////////////////////////////////////////////////////

package space.kklochko.simple_jbdd.tests.runners;

import space.kklochko.simple_jbdd.tests.Test;
import space.kklochko.simple_jbdd.tests.commands.AbstractTestCommand;
import space.kklochko.simple_jbdd.tests.factories.AbstractTestRunnerFactory;
import space.kklochko.simple_jbdd.tests.factories.TestCommandQueueFactory;
import space.kklochko.simple_jbdd.tests.factories.meta.AbstractTestCommandMetaDataFactory;
import space.kklochko.simple_jbdd.tests.factories.meta.TestCommandMetaDataFactory;
import space.kklochko.simple_jbdd.tests.loaders.AbstractNameClassLoader;
import space.kklochko.simple_jbdd.tests.loaders.SingleTestLoader;
import space.kklochko.simple_jbdd.tests.reports.AbstractReporter;

import java.util.AbstractMap;
import java.util.ArrayList;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class CLITestRunner extends AbstractTestRunner {
    private ArrayList<AbstractTestCommand<Test>> commands;
    private AbstractNameClassLoader classLoader;
    private AbstractTestRunnerFactory runnerFactory;
    private AbstractReporter reporter;

    public CLITestRunner(AbstractNameClassLoader classLoader, AbstractTestRunnerFactory runnerFactory, AbstractReporter reporter) {
        setClassLoader(classLoader);
        setRunnerFactory(runnerFactory);
        setReporter(reporter);
    }

    public boolean run() {
        commands = createCommands(load());
        ArrayList<Boolean> testStatuses = runTests(commands);
        report(createMetadata(commands, testStatuses));

        return isFailed(testStatuses);
    }

    public ArrayList<Test> load() {
        Set<Class<?>> classes = this.classLoader.load();
        ArrayList<Test> tests = new ArrayList();

        for(var aClass : classes) {
            tests.add((new SingleTestLoader(aClass.getName())).load());
        }

        return tests;
    }

    public ArrayList<AbstractTestCommand<Test>> createCommands(ArrayList<Test> tests) {
        TestCommandQueueFactory factory = new TestCommandQueueFactory(tests);
        return factory.create();
    }

    public ArrayList<Boolean> runTests(ArrayList<AbstractTestCommand<Test>> commands) {
        return commands.stream()
                       .map(c -> runnerFactory.create(c).run())
                       .collect(Collectors.toCollection(ArrayList::new));
    }

    public ArrayList<AbstractMap.SimpleEntry<AbstractTestCommandMetaDataFactory, Boolean>> createMetadata(ArrayList<AbstractTestCommand<Test>> commands, ArrayList<Boolean> testStatuses) {
        return IntStream.range(0, commands.size())
                        .mapToObj(i -> new AbstractMap.SimpleEntry<AbstractTestCommandMetaDataFactory, Boolean>(
                                            new TestCommandMetaDataFactory(commands.get(i)),
                                            testStatuses.get(i)))
                        .collect(Collectors.toCollection(ArrayList::new));
    }

    public void report(ArrayList<AbstractMap.SimpleEntry<AbstractTestCommandMetaDataFactory, Boolean>> metadata) {
        reporter.setMetadata(metadata);
        reporter.report();
    }

    public boolean isFailed(ArrayList<Boolean> testStatuses) {
        return !testStatuses.stream().anyMatch(b -> b == false);
    }

    public AbstractNameClassLoader getClassLoader() {
        return classLoader;
    }

    public void setClassLoader(AbstractNameClassLoader classLoader) {
        this.classLoader = classLoader;
    }

    public AbstractTestRunnerFactory getRunnerFactory() {
        return runnerFactory;
    }

    public void setRunnerFactory(AbstractTestRunnerFactory runnerFactory) {
        this.runnerFactory = runnerFactory;
    }

    public AbstractReporter getReporter() {
        return reporter;
    }

    public void setReporter(AbstractReporter reporter) {
        this.reporter = reporter;
    }
}

