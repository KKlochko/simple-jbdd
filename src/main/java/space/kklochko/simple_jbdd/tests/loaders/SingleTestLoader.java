package space.kklochko.simple_jbdd.tests.loaders;

import space.kklochko.simple_jbdd.tests.Test;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;

public class SingleTestLoader {
    String className;

    public SingleTestLoader(String className) {
        setClassName(className);
    }

    public Test load() {
        SingleClassLoader loader = new SingleClassLoader(getClassName());
        Class<?> aClass = loader.load();

        if(aClass == null)
            return null;

        Constructor defaultConstructor = null;

        for(Constructor constructor : aClass.getDeclaredConstructors()) {
            if(constructor.getParameterCount() == 0) {
                defaultConstructor = constructor;
                break;
            }
        }

        try {
            return (Test) defaultConstructor.newInstance();
        } catch (InstantiationException e) {
            throw new RuntimeException(e);
        } catch (IllegalAccessException e) {
            throw new RuntimeException(e);
        } catch (InvocationTargetException e) {
            throw new RuntimeException(e);
        }
    }

    public String getClassName() {
        return className;
    }

    public void setClassName(String className) {
        this.className = className;
    }
}

