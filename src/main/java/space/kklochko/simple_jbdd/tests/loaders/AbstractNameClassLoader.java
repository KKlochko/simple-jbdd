package space.kklochko.simple_jbdd.tests.loaders;

import java.util.Set;

public abstract class AbstractNameClassLoader {
    public abstract Set<Class<?>> load();
}

