package space.kklochko.simple_jbdd.tests.loaders;

import java.util.ArrayList;
import java.util.Set;

public class ClassLoader<T> {

    AbstractNameClassLoader nameLoader;

    public ClassLoader(AbstractNameClassLoader nameLoader) {
        this.nameLoader = nameLoader;
    }

    public ArrayList<T> load() {
        Set<Class<?>> names = nameLoader.load();
        ArrayList<T> classes = new ArrayList<>();

        for(Class<?> name : names) {
            SingleClassLoader loader = new SingleClassLoader(name.getName());
            classes.add((T) loader.load());
        }

        return classes;
    }
}

