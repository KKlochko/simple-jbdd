package space.kklochko.simple_jbdd.tests.loaders;

import com.google.common.reflect.ClassPath;

import java.io.IOException;
import java.util.stream.Collectors;
import java.util.Set;

public class NameClassLoader extends AbstractNameClassLoader {
    public Set<Class<?>> load() {
        try {
            return ClassPath.from(java.lang.ClassLoader.getSystemClassLoader())
                        .getAllClasses()
                        .stream()
		        // exclude the Test class, which indicate a Test class
                        .filter(aClass -> !aClass.getName().endsWith(".Test"))
                        .filter(aClass -> aClass.getName().endsWith("Test"))
		        // include classes which extends the Test class
                        .filter(aClass -> space.kklochko.simple_jbdd.tests.Test.class.isAssignableFrom(aClass.load()))
                        .filter(aClass -> aClass.getName().startsWith("space"))
                        .map(aClass -> aClass.load())
                        .collect(Collectors.toSet());
        } catch (IOException e) {
            return null;
        }
    }
}

