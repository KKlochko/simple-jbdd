package space.kklochko.simple_jbdd.tests.loaders;

public class SingleClassLoader {
    String className;

    public SingleClassLoader(String className) {
        setClassName(className);
    }

    public Class<?> load() {
        try {
            return Class.forName(getClassName());
        } catch (ClassNotFoundException e) {
            return null;
        }
    }

    public String getClassName() {
        return className;
    }

    public void setClassName(String className) {
        this.className = className;
    }
}

