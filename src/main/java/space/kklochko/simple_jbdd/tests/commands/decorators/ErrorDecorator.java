//////////////////////////////////////////////////////////////////////////////////
// Copyright (C) 2023 Kostiantyn Klochko <kklochko@protonmail.com>              //
//                                                                              //
// This file is part of simple-jbdd.                                            //
//                                                                              //
// simple-jbdd is free software: you can redistribute it and/or modify it under //
// the terms of the GNU Lesser General Public License as published by the Free  //
// Software Foundation, either version 3 of the License, or (at your option)    //
// any later version.                                                           //
//                                                                              //
// simple-jbdd is distributed in the hope that it will be useful, but WITHOUT   //
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or        //
// FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License  //
// for more details.                                                            //
//                                                                              //
// You should have received a copy of the GNU Lesser General Public License     //
// along with simple-jbdd. If not, see <https://www.gnu.org/licenses/>.         //
//////////////////////////////////////////////////////////////////////////////////

package space.kklochko.simple_jbdd.tests.commands.decorators;

import space.kklochko.simple_jbdd.tests.Test;
import space.kklochko.simple_jbdd.tests.commands.AbstractTestCommand;

public class ErrorDecorator<T extends Test> extends AbstractDecorator<T> {
    public ErrorDecorator(String error_label, AbstractTestCommand<T> command, String type) {
        super(error_label, command);
        setType(type);
    }

    public boolean test() {
        return false;
    }

    public boolean runMethod() {
        return false;
    };
}

