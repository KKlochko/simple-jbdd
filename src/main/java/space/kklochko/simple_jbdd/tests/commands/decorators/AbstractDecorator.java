//////////////////////////////////////////////////////////////////////////////////
// Copyright (C) 2023 Kostiantyn Klochko <kklochko@protonmail.com>              //
//                                                                              //
// This file is part of simple-jbdd.                                            //
//                                                                              //
// simple-jbdd is free software: you can redistribute it and/or modify it under //
// the terms of the GNU Lesser General Public License as published by the Free  //
// Software Foundation, either version 3 of the License, or (at your option)    //
// any later version.                                                           //
//                                                                              //
// simple-jbdd is distributed in the hope that it will be useful, but WITHOUT   //
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or        //
// FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License  //
// for more details.                                                            //
//                                                                              //
// You should have received a copy of the GNU Lesser General Public License     //
// along with simple-jbdd. If not, see <https://www.gnu.org/licenses/>.         //
//////////////////////////////////////////////////////////////////////////////////

package space.kklochko.simple_jbdd.tests.commands.decorators;

import space.kklochko.simple_jbdd.tests.Test;
import space.kklochko.simple_jbdd.tests.commands.AbstractTestCommand;

import java.lang.reflect.Method;
import java.util.Map;

public abstract class AbstractDecorator<T extends Test> extends AbstractTestCommand<T> {
    protected AbstractTestCommand<T> command;

    protected Method method;

    public AbstractDecorator(String label, AbstractTestCommand<T> command) {
        super(label);
        setCommand(command);
    }

    public abstract boolean test();

    public Map<String, String> getLabels() {
        Map<String, String> labels = getCommand().getLabels();
        labels.put(getType(), this.getLabel());
        return labels;
    }

    public abstract boolean runMethod();

    public void setMethod(Method method) {
        this.method = method;
    }

    public Method getMethod() {
        return method;
    }

    @Override
    public void setObject(T object) {
        command.setObject(object);
    }

    @Override
    public T getObject() {
        return command.getObject();
    }

    public AbstractTestCommand<T> getCommand() {
        return command;
    }

    public void setCommand(AbstractTestCommand<T> command) {
        this.command = command;
    }
}

