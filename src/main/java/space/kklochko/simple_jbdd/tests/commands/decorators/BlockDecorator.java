//////////////////////////////////////////////////////////////////////////////////
// Copyright (C) 2023 Kostiantyn Klochko <kklochko@protonmail.com>              //
//                                                                              //
// This file is part of simple-jbdd.                                            //
//                                                                              //
// simple-jbdd is free software: you can redistribute it and/or modify it under //
// the terms of the GNU Lesser General Public License as published by the Free  //
// Software Foundation, either version 3 of the License, or (at your option)    //
// any later version.                                                           //
//                                                                              //
// simple-jbdd is distributed in the hope that it will be useful, but WITHOUT   //
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or        //
// FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License  //
// for more details.                                                            //
//                                                                              //
// You should have received a copy of the GNU Lesser General Public License     //
// along with simple-jbdd. If not, see <https://www.gnu.org/licenses/>.         //
//////////////////////////////////////////////////////////////////////////////////

package space.kklochko.simple_jbdd.tests.commands.decorators;

import space.kklochko.simple_jbdd.tests.Test;
import space.kklochko.simple_jbdd.tests.commands.AbstractTestCommand;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class BlockDecorator<T extends Test> extends AbstractDecorator<T> {
    public BlockDecorator(String label, AbstractTestCommand<T> command, Method method, String type) {
        super(label, command);
        setType(type);
        setMethod(method);
    }

    public boolean test() {
        boolean result = runMethod();

        if(!result)
            return false;

        return getCommand().test();
    }

    public boolean runMethod() {
        try {
            method.invoke(getCommand().getObject());
            return true;
        } catch (InvocationTargetException e) {
            Throwable exception = e.getCause();

            if(exception instanceof AssertionError)
                throw (AssertionError) exception;

            System.err.println(e.getMessage());
            throw new RuntimeException(e);
        } catch (IllegalAccessException e) {
            System.err.println("Check access modifiers.");
            throw new RuntimeException(e);
        }
    };
}

