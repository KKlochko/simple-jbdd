# Change Log

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

## [0.13.5] - 2023-11-02

### Fixed
- Fix the typo in the README.

### Changed
- Update the version.
- Update the CHANGELOG.

## [0.13.4] - 2023-10-19

### Changed
- Update the version.
- Update the CHANGELOG.
- Update the CI/CD configuration to publish the artifacts to the release.

## [0.13.3] - 2023-10-19

### Fixed
- Fix the validators's test have two variant of the order for methods.

## [0.13.2] - 2023-10-18

### Changed
- Update the README.
- Update the version.
- Update the CHANGELOG.

## [0.13.1] - 2023-10-18

### Changed
- Add the license, LGPLv3.

## [0.13.0] - 2023-10-18

### Added
- Add validators to show error if a test method is invalid.
- Add a Calculator and its tests.
- Add the invalid tests.
- Add the tests for test validators.

### Fixed
- Fix the given-when-then tests to have public methods.
- Fix the test count for ClassLoader's test.

### Changed
- Update the block to run only a public method.
- Update the TestCommandFactory to have the validators.

## [0.12.7] - 2023-10-16

### Added
- Add the CHANGELOG.

## [0.12.6] - 2023-10-16

### Fixed
- Fix the project name for the gradle build.

## [0.12.5] - 2023-10-15

### Changed
- Update the build configuration to make the JAR file.

## [0.12.4] - 2023-10-15

### Added
- Add SimpleEmptyTest and its tests.

## [0.12.3] - 2023-10-15

### Added
- Add the error for an empty test.

## [0.12.2] - 2023-10-15

### Changed
- Update the titles for SimpleFailedThenTest and SimpleGivenWhenThenTest.

## [0.12.1] - 2023-10-15

### Added
- Add a multi-block test.

### Changed
- Update the tests to test the multi-block test.

## [0.12.0] - 2023-10-15

### Added
- Add the multi-block tests support.

## [0.11.2] - 2023-10-10

### Added
- Add the main entry point.

## [0.11.1] - 2023-10-10

### Added
- Add a constructor to set only a formatter for CLIReporter.

## [0.11.0] - 2023-10-10

### Added
- Add the CLITestRunner to run tests and to report the result.

## [0.10.1] - 2023-10-10

### Changed
- Update the CLI test formatting.

## [0.10.0] - 2023-10-8

### Added
- Add the SingleTestLoader to create an object of a loaded Test class.

## [0.9.2] - 2023-10-8

### Added
- Add a constructor with a default factory for TestCommandQueueFactory.

## [0.9.1] - 2023-10-8

### Fixed
- Fix the test title for SingleTestClassLoader.

## [0.9.0] - 2023-10-7

### Added
- Add the AbstractReporter.
- Add the CLIReporter and its tests.

## [0.8.3] - 2023-10-7

### Added
- Add Apache Commons Lang library.
- Add default constructor for SimpleTestReportFormatter.

## [0.8.2] - 2023-10-6

### Added
- Add the AbstractTestReportFormatter.

### Changed
- Refactor the SimpleTestReportFormatter to use the interface.

## [0.8.1] - 2023-10-6

### Fixed
- Fix the encoding issue.

## [0.8.0] - 2023-10-4

### Added
- Add the SimpleTestReportFormatter.

## [0.7.0] - 2023-10-3

### Added
- Add the TestCommandMetaDataFactory to generate the metadata.

## [0.6.3] - 2023-10-1

### Added
- Add the ClassLoader.

## [0.6.2] - 2023-10-1

### Fixed
- Fix typos and improve description in the tests.

## [0.6.1] - 2023-09-30

### Added
- Add the com.google.guava as a dependency.
- Add the NameClassLoader.

## [0.6.0] - 2023-09-29

### Added
- Add the single class loader.

## [0.5.4] - 2023-09-27

### Added
- Add the queue factory for test commands.

## [0.5.3] - 2023-09-26

### Added
- Add Abstract and Simple factories to create a test runner.

## [0.5.2] - 2023-09-26

### Changed
- Refactor tests for the SimpleTestRunner.
- Refactor the SimpleTestRunner and its test.

## [0.5.1] - 2023-09-24

### Fixed
- Fix the BlockDecorator to throw the AsssertionError, if any.

## [0.5.0] - 2023-09-24

### Added
- Add SimpleTestRunner as a test runner.

## [0.4.1] - 2023-09-23

### Added
- Add the CI/CD configuration for Gradle and Maven build.

## [0.4.0] - 2023-09-23

### Added
- Add the create method for the TestCommandFactory.

## [0.3.1] - 2023-09-23

### Added
- Add the test block decorator.

## [0.3.0] - 2023-09-23

### Added
- Add abstract and simple test commands.

## [0.2.7] - 2023-09-23

### Added
- Add a test example without a title annotation.

## [0.2.6] - 2023-09-23

### Changed
- Update test examples to have annotations with values.

## [0.2.5] - 2023-09-23

### Added
- Add the Title annotation.

## [0.2.4] - 2023-09-20

### Added
- Add the TestCommandFactory with a getTestMethods.

## [0.2.3] - 2023-09-20

### Added
- Add the Test class as ABC.

## [0.2.2] - 2023-09-20

### Added
- Add test examples.

## [0.2.1] - 2023-09-20

### Changed
- Refactor the Sum class to be moved into the samples package.

## [0.2.0] - 2023-09-19

### Added
- Add the Given-When-Then annotations.

## [0.1.2] - 2023-09-19

### Removed
- Remove the test examples.

## [0.1.1] - 2023-09-19

### Added
- Add the Sum class to test as an example.

## [0.1.0] - 2023-09-19

### Added
- Project initialization.

[Unreleased]: https://gitlab.com/KKlochko/cipher-analytical-machine/-/compare/0.13.5...main?from_project_id=50218541&straight=false
[0.13.5]: https://gitlab.com/KKlochko/simple-jbdd/-/compare/0.13.4...0.13.5?from_project_id=50536015&straight=false
[0.13.4]: https://gitlab.com/KKlochko/simple-jbdd/-/compare/0.13.3...0.13.4?from_project_id=50536015&straight=false
[0.13.3]: https://gitlab.com/KKlochko/simple-jbdd/-/compare/0.13.2...0.13.3?from_project_id=50536015&straight=false
[0.13.2]: https://gitlab.com/KKlochko/simple-jbdd/-/compare/0.13.1...0.13.2?from_project_id=50536015&straight=false
[0.13.1]: https://gitlab.com/KKlochko/simple-jbdd/-/compare/0.13.0...0.13.1?from_project_id=50536015&straight=false
[0.13.0]: https://gitlab.com/KKlochko/simple-jbdd/-/compare/0.12.7...0.13.0?from_project_id=50536015&straight=false
[0.12.7]: https://gitlab.com/KKlochko/simple-jbdd/-/compare/0.12.6...0.12.7?from_project_id=50536015&straight=false
[0.12.6]: https://gitlab.com/KKlochko/simple-jbdd/-/compare/0.12.5...0.12.6?from_project_id=50536015&straight=false
[0.12.5]: https://gitlab.com/KKlochko/simple-jbdd/-/compare/0.12.4...0.12.5?from_project_id=50536015&straight=false
[0.12.4]: https://gitlab.com/KKlochko/simple-jbdd/-/compare/0.12.3...0.12.4?from_project_id=50536015&straight=false
[0.12.3]: https://gitlab.com/KKlochko/simple-jbdd/-/compare/0.12.2...0.12.3?from_project_id=50536015&straight=false
[0.12.2]: https://gitlab.com/KKlochko/simple-jbdd/-/compare/0.12.1...0.12.2?from_project_id=50536015&straight=false
[0.12.1]: https://gitlab.com/KKlochko/simple-jbdd/-/compare/0.12.0...0.12.1?from_project_id=50536015&straight=false
[0.12.0]: https://gitlab.com/KKlochko/simple-jbdd/-/compare/0.11.2...0.12.0?from_project_id=50536015&straight=false
[0.11.2]: https://gitlab.com/KKlochko/simple-jbdd/-/compare/0.11.1...0.11.2?from_project_id=50536015&straight=false
[0.11.1]: https://gitlab.com/KKlochko/simple-jbdd/-/compare/0.11.0...0.11.1?from_project_id=50536015&straight=false
[0.11.0]: https://gitlab.com/KKlochko/simple-jbdd/-/compare/0.10.1...0.11.0?from_project_id=50536015&straight=false
[0.10.1]: https://gitlab.com/KKlochko/simple-jbdd/-/compare/0.10.0...0.10.1?from_project_id=50536015&straight=false
[0.10.0]: https://gitlab.com/KKlochko/simple-jbdd/-/compare/0.9.2...0.10.0?from_project_id=50536015&straight=false
[0.9.2]: https://gitlab.com/KKlochko/simple-jbdd/-/compare/0.9.1...0.9.2?from_project_id=50536015&straight=false
[0.9.1]: https://gitlab.com/KKlochko/simple-jbdd/-/compare/0.9.0...0.9.1?from_project_id=50536015&straight=false
[0.9.0]: https://gitlab.com/KKlochko/simple-jbdd/-/compare/0.8.3...0.9.0?from_project_id=50536015&straight=false
[0.8.3]: https://gitlab.com/KKlochko/simple-jbdd/-/compare/0.8.2...0.8.3?from_project_id=50536015&straight=false
[0.8.2]: https://gitlab.com/KKlochko/simple-jbdd/-/compare/0.8.1...0.8.2?from_project_id=50536015&straight=false
[0.8.1]: https://gitlab.com/KKlochko/simple-jbdd/-/compare/0.8.0...0.8.1?from_project_id=50536015&straight=false
[0.8.0]: https://gitlab.com/KKlochko/simple-jbdd/-/compare/0.7.0...0.8.0?from_project_id=50536015&straight=false
[0.7.0]: https://gitlab.com/KKlochko/simple-jbdd/-/compare/0.6.3...0.7.0?from_project_id=50536015&straight=false
[0.6.3]: https://gitlab.com/KKlochko/simple-jbdd/-/compare/0.6.2...0.6.3?from_project_id=50536015&straight=false
[0.6.2]: https://gitlab.com/KKlochko/simple-jbdd/-/compare/0.6.1...0.6.2?from_project_id=50536015&straight=false
[0.6.1]: https://gitlab.com/KKlochko/simple-jbdd/-/compare/0.6.0...0.6.1?from_project_id=50536015&straight=false
[0.6.0]: https://gitlab.com/KKlochko/simple-jbdd/-/compare/0.5.4...0.6.0?from_project_id=50536015&straight=false
[0.5.4]: https://gitlab.com/KKlochko/simple-jbdd/-/compare/0.5.3...0.5.4?from_project_id=50536015&straight=false
[0.5.3]: https://gitlab.com/KKlochko/simple-jbdd/-/compare/0.5.2...0.5.3?from_project_id=50536015&straight=false
[0.5.2]: https://gitlab.com/KKlochko/simple-jbdd/-/compare/0.5.1...0.5.2?from_project_id=50536015&straight=false
[0.5.1]: https://gitlab.com/KKlochko/simple-jbdd/-/compare/0.5.0...0.5.1?from_project_id=50536015&straight=false
[0.5.0]: https://gitlab.com/KKlochko/simple-jbdd/-/compare/0.4.1...0.5.0?from_project_id=50536015&straight=false
[0.4.1]: https://gitlab.com/KKlochko/simple-jbdd/-/compare/0.4.0...0.4.1?from_project_id=50536015&straight=false
[0.4.0]: https://gitlab.com/KKlochko/simple-jbdd/-/compare/0.3.1...0.4.0?from_project_id=50536015&straight=false
[0.3.1]: https://gitlab.com/KKlochko/simple-jbdd/-/compare/0.3.0...0.3.1?from_project_id=50536015&straight=false
[0.3.0]: https://gitlab.com/KKlochko/simple-jbdd/-/compare/0.2.7...0.3.0?from_project_id=50536015&straight=false
[0.2.7]: https://gitlab.com/KKlochko/simple-jbdd/-/compare/0.2.6...0.2.7?from_project_id=50536015&straight=false
[0.2.6]: https://gitlab.com/KKlochko/simple-jbdd/-/compare/0.2.5...0.2.6?from_project_id=50536015&straight=false
[0.2.5]: https://gitlab.com/KKlochko/simple-jbdd/-/compare/0.2.4...0.2.5?from_project_id=50536015&straight=false
[0.2.4]: https://gitlab.com/KKlochko/simple-jbdd/-/compare/0.2.3...0.2.4?from_project_id=50536015&straight=false
[0.2.3]: https://gitlab.com/KKlochko/simple-jbdd/-/compare/0.2.2...0.2.3?from_project_id=50536015&straight=false
[0.2.2]: https://gitlab.com/KKlochko/simple-jbdd/-/compare/0.2.1...0.2.2?from_project_id=50536015&straight=false
[0.2.1]: https://gitlab.com/KKlochko/simple-jbdd/-/compare/0.2.0...0.2.1?from_project_id=50536015&straight=false
[0.2.0]: https://gitlab.com/KKlochko/simple-jbdd/-/compare/0.1.2...0.2.0?from_project_id=50536015&straight=false
[0.1.2]: https://gitlab.com/KKlochko/simple-jbdd/-/compare/0.1.1...0.1.2?from_project_id=50536015&straight=false
[0.1.1]: https://gitlab.com/KKlochko/simple-jbdd/-/compare/0.1.0...0.1.1?from_project_id=50536015&straight=false
[0.1.0]: https://gitlab.com/KKlochko/simple-jbdd/-/commit/dabb09c518cdfad4aa05ac9d86c857833b8db41c
